#!/bin/bash

declare -a docker_imgs
docker_imgs=(
    python:latest
    python:slim-buster
    python:3.9-slim-buster
    python:3.8-slim-buster
    python:3.8-slim
    python:3.7-slim-stretch
    python:3.7-slim-buster
    quay.io/python-devs/ci-image
    gcr.io/google-appengine/python:latest
    debian:stretch-slim
    debian:stable-slim
    debian:sid-slim
    debian:unstable-slim
    debian:testing-backports
    debian:stretch-backports
    debian:stable-backports
    debian:buster-backports
    bitnami/python:latest
    bitnami/python:3.7
    pypy:latest
    pypy:buster
    pypy:3.7
)
### Testing only
# docker_imgs=( gcr.io/google-appengine/python:latest )
export docker_imgs

function img_pull() {
for img in "${docker_imgs[@]}"; do
    docker pull "${img}"
done
}

function img_inspect() {
for img in "${docker_imgs[@]}"; do
    ### HDR
    /bin/echo -ne "### ${img}\t"
    ### Image Size
    docker inspect "${img}" \
    | jq -M .[0].Size \
    | gawk '{magn=int(log(int($1))/log(10)); print $1/1024/1024,"MBytes"}'
    ### TODO; human units of size

    VER_MAJORS=( python python2 python3 )
    VER_TWOS=( $(echo python2.{0..7}) )
    VER_THREES=( $(echo python3.{0..9}) )
    PYTHONS=( "${VER_MAJORS[@]}" "${VER_TWOS[@]}" "${VER_THREES[@]}" )
    for py in "${PYTHONS[@]}"; do
        ### assembling all the different versions of python possible to invoke as
        ### single command passed to Docker run
        CMD+="${py} --version;"
    done
    /bin/echo -ne "## Versions of Python installed in base image\n"
    docker run --rm "${img}" bash -c "${CMD}" 2>/dev/null \
    | sort --version-sort --unique
    ### TODO: something similar for path of each version

    /bin/echo -e "\n## Installable versions of Python:"
    docker run --rm "${img}" bash -c "apt-get -d -qq update && apt-cache search ^python[23][.0..9]*$ | awk -F' - ' '{print $1}'"

    /bin/echo -e "\n## Number of outstanding patches of the base image"
    docker run --rm "${img}" bash -c "apt-get -d -qq update && apt list --upgradable 2>/dev/null| wc -l"

    ### TODO: number of installed pkgs
    ### TODO: version of PIP for each Py version installed
    ### TODO: coallesce the ops that need apt-get update for speed
done
}


# img_pull
img_inspect
