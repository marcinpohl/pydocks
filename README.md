# Docker images for Python
Python image vary in quality, size, freshness, comprehensiveness, etc.  This is a summary helping you pick the least annoying one

## Official Python based images
 * `docker pull python:latest`
 * `docker pull python:slim-buster`
 * `docker pull python:3.9-slim-buster`
 * `docker pull python:3.8-slim-buster`
 * `docker pull python:3.8-slim`
 * `docker pull python:3.7-slim-stretch`
 * `docker pull python:3.7-slim-buster`
 * `docker pull quay.io/python-devs/ci-image` seems to be python-current with [instructions](https://gitlab.com/python-devs/ci-images/-/blob/master/get-pythons.sh)


## Google Cloud images
Python 2.7, 3.5, 3.6, 3.7

[Google runtime python build scripts](https://github.com/GoogleCloudPlatform/python-runtime/tree/master/python-interpreter-builder/scripts)



### Docker pull command

```bash
docker pull gcr.io/google-appengine/python:latest
```

## Debian based images
 * `docker pull debian:stretch-slim`
 * `docker pull debian:stable-slim`
 * `docker pull debian:sid-slim`
 * `docker pull debian:unstable-slim`
 * `docker pull debian:testing-backports`
 * `docker pull debian:stretch-backports`
 * `docker pull debian:stable-backports`
 * `docker pull debian:buster-backports`

## [Bitnami based images](https://github.com/bitnami/bitnami-docker-python)
Good instructions on GH.

Some interesting features like [building the image yourself](docker build -t bitnami/python 'https://github.com/bitnami/bitnami-docker-python.git#master:3.7/debian-10')
 * `docker pull bitnami/python:latest`
 * `docker pull bitnami/python:3.7`


## Django image
**Deprecated for the sake of standard python image**


## PyPy images
 * `docker pull pypy:latest`
 * `docker pull pypy:buster`
 * `docker pull pypy:3.7`
